angular.module 'home' .controller 'home', ['$rootScope', '$scope', ($rootScope, $scope)!->
  
  # ====== 1 $scope变量初始化 ======
  init-scope-variable = !->

  # ====== 2 $rootScope变量初始化 ======
  init-rootScope-variable = !->
    $rootScope.isUserLogin = true
  
  # ====== 3 $resource变量初始化 ======
  init-resource-variable = !->

  # ====== 4 页面元素初始化 ======
  init-page-dom = !->

  # ====== 5 工具函数定义 ======
  init-home = !->
    init-scope-variable!
    init-rootScope-variable!
    init-resource-variable!
    init-page-dom!

  # ====== 6 $scope事件函数定义 ======

  # ====== 7 初始化函数执行 ======
  init-home!
]