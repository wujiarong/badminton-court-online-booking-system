angular.module 'login-register' .controller 'login-register', ['$rootScope', '$scope', '$resource', '$location', 'md5', ($rootScope, $scope, $resource, $location, md5)!->
  # ====== 1 $scope变量初始化 ======
  init-scope-variable = !->
    $scope.tab = {}
    $scope.tab.select = 'login'

    $scope.user = {}
    $scope.user.username = 'zhangsan'
    $scope.user.password = '123456'

  # ====== 2 $rootScope变量初始化 ======
  init-rootScope-variable = !->
    $rootScope.user = {}

  # ====== 3 $resource变量初始化 ======
  init-resource-variable = !->
    $scope.resource = {}
    $scope.resource.one-user = $resource '/one-user'

  # ====== 4 页面元素初始化 ======
  init-page-dom = !->

  # ====== 5 工具函数定义 ======
  init-login-register = !->
    init-scope-variable!
    init-rootScope-variable!
    init-resource-variable!

  # ====== 6 $scope事件函数定义 ======
  $scope.select-login-tab = (event)!->
    $scope.tab.select = 'login'
    $ '.register-tab' .remove-class 'choose'
    $ '.login-tab' .add-class 'choose'

  $scope.select-register-tab = (event)!->
    $scope.tab.select = 'register'
    $ '.login-tab' .remove-class 'choose'
    $ '.register-tab' .add-class 'choose'

  $scope.login-one-user = (event)!->
    query-data = 
      username: $scope.user.username
      password: md5.create-hash $scope.user.password

    result = $scope.resource.one-user.get query-data, !->
      if result.err 
        alert result.msg
      else
        $rootScope.user = result.data 
        $location.path '/' + $rootScope.user.username

  $scope.register-one-user = (event)!->
    if $scope.user.password isnt $scope.user.re-password
      alert '两次密码不一致'
      return 

    post-data = 
      method: 'CREATE'
      username: $scope.user.username
      password: md5.create-hash $scope.user.password

    result = $scope.resource.one-user.save {}, post-data, !->
      if result.err
        alert result.msg
      else
        $scope.login-one-user!

  # ====== 7 初始化函数执行 ======
  init-login-register!
  # $scope.login-one-user!
]