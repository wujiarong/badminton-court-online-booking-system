angular.module 'side-menu' .controller 'side-menu', ['$rootScope', '$scope', '$location', ($rootScope, $scope, $location)!->
  # ====== 1 $scope变量初始化 ======
  init-scope-variable = !->
    $scope.name = '陈晓雅'

  # ====== 2 $rootScope变量初始化 ======
  init-rootScope-variable = !->
    $rootScope.isUserLogin = false
  
  # ====== 3 $resource变量初始化 ======
  init-resource-variable = !->

  # ====== 4 页面元素初始化 ======
  init-page-dom = !->
    init-menu-list!

  init-menu-list = !->
    $ '.menu-list li a' .on 'click', (event)!->
      $ '.menu-list li a' .remove-class 'choose'
      $ event.target .add-class 'choose'
      console.log 'hahah'

  # ====== 5 工具函数定义 ======
  init-side-menu = !->
    init-scope-variable!
    init-rootScope-variable!
    init-resource-variable!
    init-page-dom!

  # ====== 6 $scope事件函数定义 ======
  $scope.logout = (event)!->
    $ '.menu-list li a' .remove-class 'choose'
    $ ($ '.menu-list li a' .0) .add-class 'choose'

    $rootScope.isUserLogin = false
    $rootScope.user = null
    $location.path '/'

  # ====== 7 初始化函数执行 ======
  init-side-menu!

]