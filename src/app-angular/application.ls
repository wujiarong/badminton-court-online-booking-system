# 定义matrix模块

ng-app = 'badminton'
ng-app-module = angular.module ng-app, [
  'ngRoute', 
  'ngResource',
  'login-register',
  'side-menu',
  'angular-md5',
  'home',
  'court-manage',
  'my-court',
  'order-manage',
  'profile'
]

ng-app-module.config ['$resourceProvider', ($resourceProvider)->
  # Don't strip trailing slashes from calculated URLs
  $resourceProvider.defaults.stripTrailingSlashes = false;
]

# 配置location服务
ng-app-module.config ['$locationProvider', ($locationProvider)->
  $locationProvider.html5-mode {
    enabled: true
    require-base: true
    rewrite-links: true
  }
]

# 设置rootScope变量；为了方便查看和统一管理，在各个子controller里用到的rootScope变量需要在这里定义和说明
ng-app-module .controller 'badminton', ['$rootScope', ($rootScope)!->
  $rootScope.isUserLogin = false
]

# 应用路由
angular.module ng-app .config ['$routeProvider', ($routeProvider)!->
  $routeProvider.
  when('/login-register', {
    template-url: 'app-angular/login-register/views/login-register.client.view.html'
  }).
  when('/:username', {
    template-url: 'app-angular/home/views/home.client.view.html'
  }).
  when('/:username/my-court', {
    template-url: 'app-angular/my-court/views/my-court.client.view.html'
  }).
  when('/:username/court-manage', {
    template-url: 'app-angular/court-manage/views/court-manage.client.view.html'
  }).
  when('/:username/profile', {
    template-url: 'app-angular/profile/views/profile.client.view.html'
  }).
  when('/:username/order-manage', {
    template-url: 'app-angular/order-manage/views/order-manage.client.view.html'
  }).
  otherwise({
    redirect-to: '/login-register'
  })
]

# 手动引导ng-app
angular.element document .ready !->
  angular.bootstrap document, [ng-app]


  # # ====== 1 $scope变量初始化 ======
  # init-scope-variable = !->

  # # ====== 2 $rootScope变量初始化 ======
  # init-rootScope-variable = !->
  
  # # ====== 3 $resource变量初始化 ======
  # init-resource-variable = !->

  # # ====== 4 页面元素初始化 ======
  # init-page-dom = !->

  # # ====== 5 工具函数定义 ======
  # init- = !->

  # # ====== 6 $scope事件函数定义 ======

  # # ====== 7 初始化函数执行 ======
  # init-!