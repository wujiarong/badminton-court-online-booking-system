module.exports = (app)!->
  controller = require('../controllers/login-register.server.controller');
  app.get '/one-user', controller.retrieve-one-user
  app.post '/one-user', controller.create-or-update-one-user
