# 配置express各种中间件

config = require './config' # 配置信息
express = require 'express' # express
morgan = require 'morgan' # 服务端输出工具
compress = require 'compression' # response body压缩
body-parser = require 'body-parser' # request body parser中间件
method-override = require 'method-override' # 对某些不支持的http方法进行重写
session = require 'express-session' # session
flash = require 'connect-flash' # flash
mysql-store = require('express-mysql-session') session # mysql session存储
session-store = new mysql-store(config.sess-store) # 创建session存储

fs = require 'fs'
path = require 'path'

module.exports = ->
  # express变量
  app = express!

  app.use compress!
  app.use body-parser.urlencoded({extended: true})
  app.use body-parser.json!
  app.use method-override!

  app.use session opts =
    name: 'matrix-session-id'
    store: session-store
    save-uninitialized: true
    resave: false
    secret: config.session-secret
    cookie: opts =
      path: '/'
      http-only: true
      secure: false
      max-age: 20 * 60 * 1000

  app.use flash!

  app.use express.static './'
  app.use express.static './public'

  require('../routers/index.server.router') app
  require('../routers/login-register.server.router') app

  app.get '/*', (req, res)!-> res.redirect '/'

  app
