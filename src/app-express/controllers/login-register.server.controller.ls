mysql = require 'mysql'
config = require '../config/config'

exports.retrieve-one-user = (req, res)!->
  conn = mysql.createConnection config.db
  sql = 'SELECT * FROM user WHERE username="' + req.query.username + '" AND password="' + req.query.password + '";'

  conn.query sql, (err, results)!->
    if err 
      console.log 'Function: login-one-user, error: ', err 
      res.json response = 
        err: true
        msg: '登录失败，数据库查询发生错误'
    else
      if results.length is 0
        res.json response = 
          err: true
          msg: '登录失败，用户名或密码错误'
      else
        data = results[0]
        delete data.password
        res.json response = 
          err: false
          data: data

    conn.end!

create-one-user = (req, res)!->
  conn = mysql.createConnection config.db

  select-sql = 'SELECT * FROM user WHERE username="' + req.body.username + '";'
  conn.query select-sql, (err, select-results)!->
    if err 
      res.json response = 
        err: true 
        msg: '注册失败，数据库查询错误'
      conn.end!
    else if select-results.length isnt 0
      res.json response = 
        err: true
        msg: '注册失败，用户已经存在'
      conn.end!
    else
      insert-sql = 'INSERT INTO user SET ?'
      values = 
        username: req.body.username
        password: req.body.password
      
      conn.query insert-sql, values, (err, insert-results)!->
        if err 
          console.log 'Function: create-one-user, error: ', err
          res.json response = 
            err: true 
            msg: '注册失败，数据库插入错误'
        else
          res.json response =
            err: false
            data: null 

        conn.end!

update-one-user = (req, res)!->
  conn = mysql.createConnection config.db
  sql = 'UPDATE user SET ? WHERE id="' + req.body.id + '";'
  values = 
    username: req.query.username
    password: req.query.password
  conn.query sql, values, (err, results)!->
    console.log err, results
    res.json response = 
      err: true
      msg: '更新失败'
    conn.end!

exports.create-or-update-one-user = (req, res)!->
  if req.body.method is 'CREATE'
    create-one-user req, res 
  else if req.body.method is 'UPDATE'
    update-one-user req, res
