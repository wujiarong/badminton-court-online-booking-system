app = require('./app-express/config/express')!
config = require './app-express/config/config'

app.get '/', !->
  res.send 'Hello World!'

app.listen 3000, !->
  console.log 'Example app listening on port ' + config.port

module.exports = app
