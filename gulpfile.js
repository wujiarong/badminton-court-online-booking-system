'use strict';

// 开发时运行命令：gulp serve
// 本地部署时运行命令：gulp build:local
// 远程部署时运行命令：gulp build:deploy

var gulp = require('gulp'),
    gulpLiveScript = require('gulp-livescript'),
    sourcemaps = require('gulp-sourcemaps'),
    gutil = require('gutil'),
    gulpJade = require('gulp-jade'),
    del = require('del'),
    jade = require('jade'),
    sass = require('gulp-sass'),
    server = require('gulp-develop-server'),
    browserSync = require('browser-sync').create(),
    inject = require('gulp-inject'),
    cleanCSS = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    rev = require('gulp-rev'),
    uglify = require('gulp-uglify'),
    htmlmin = require('gulp-htmlmin'),
    imagemin = require('gulp-imagemin'),
    exec = require('child_process').exec;

gulp.task('clean:code', function() {
  del(['./app-*/', './server.js', './build/*']);
});

gulp.task('sass', ['clean:code'], function () {
  return gulp.src('./src/**/*.sass')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(gulp.dest('./'));
});

gulp.task('jade', ['sass'], function() {
  return gulp.src('./src/**/*.jade')
    .pipe(sourcemaps.init())
    .pipe(gulpJade({
      jade: jade,
      pretty: true
    }))
    .pipe(gulp.dest('./'));
});

gulp.task('ls', ['jade'], function() {
  gulp.src('./src/server.ls')
    .pipe(gulpLiveScript({bare: true}))
    .pipe(gulp.dest('./'));

  return gulp.src('./src/app-*/**/*.ls')
    .pipe(sourcemaps.init())
    .pipe(gulpLiveScript({bare: true})
    .on('error', gutil.log))
    .pipe(gulp.dest('./'));
});

gulp.task('inject', ['ls'], function() {
  var target = gulp.src('./app-angular/application.html');
  var cssFiles = [
    './app-angular/**/*.css',
    './bower_components/font-awesome/css/font-awesome.min.css',
    './bower_components/bootstrap/dist/css/bootstrap.min.css'
  ];
  var jsFiles = [
    './bower_components/angular/angular.min.js',
    './bower_components/angular-route/angular-route.min.js',
    './bower_components/angular-resource/angular-resource.min.js',
    './bower_components/angular-md5/angular-md5.min.js',
    './bower_components/jquery/dist/jquery.min.js',
    './bower_components/bootstrap/dist/js/bootstrap.min.js',
    './app-angular/**/*.js'
  ];
  var sources = gulp.src(cssFiles.concat(jsFiles), {read: false});
  return target.pipe(inject(sources))
    .pipe(gulp.dest('./app-angular'));
});

gulp.task('browser:run', function() {
  browserSync.init({
    proxy: "http://localhost:3000"
  });
});

gulp.task('server:start', ['inject'], function() {
  server.listen({path: './server.js'}, function() {
    gulp.start('browser:run');
  });
});

gulp.task('browser:reload', function() {
  browserSync.reload();
});

gulp.task('server:restart', ['inject'], function() {
  server.restart(function() {
    gulp.start('browser:reload');
  });
});

gulp.task('serve', ['server:start'], function() {
  gulp.watch(['src/app-angular/**/*.*', 'src/app-express/**/*.*', 'src/server.ls'], ['server:restart']);
});

gulp.task('minify-css', ['ls'], function() {
  return gulp.src('app-angular/**/*.css')
    .pipe(autoprefixer())
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(concat('matrix.min.css'))
    .pipe(gulp.dest('build/bin/css/'))
    .pipe(rev())
    .pipe(gulp.dest('build/rev/css/'))
    .pipe(rev.manifest({merge: true}))
    .pipe(gulp.dest('build/assets/rev/css'));
});

gulp.task('minify-js', ['minify-css'], function() {
  return gulp.src('app-angular/**/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('build/bin/js/'))
    .pipe(rev())
    .pipe(gulp.dest('build/rev/js/'))
    .pipe(rev.manifest({merge: true}))
    .pipe(gulp.dest('build/assets/rev/js'));
});

gulp.task('minify-html', ['minify-js'], function() {
  return gulp.src('app-angular/**/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('app-angular/'));
});

gulp.task('minify-img', ['minify-html'], function() {
  return gulp.src('public/images/*')
    .pipe(imagemin())
    .pipe(gulp.dest('public/images'));
});
